// Extend from VM as this is a particular application view.
Prodea.App.sample.Controls.dashboard = $.extend( new Prodea.VM.Base(), {
	// This is the init function hfsfj sjfbhsf sjfhf shjsfs ghsg
    init: function() {

        this.updateDashboard();
    
        setTimeout( $.proxy( function() { this.parent.loaded(); }, this), 1 ); // Trigger this 'out of sync'

	$("#button").click(function () {
	
    // Set the effect type
    var effect = 'slide';

    // Set the options for the effect type chosen
    var options = { direction: 'right' };

    // Set the duration (default: 400 milliseconds)
    var duration = 1500;

    $('#myDiv').toggle(effect, options, duration);
	Prodea.Nav.focus("#addToCart");

	});
	$("#addToCart").click(function (){
		Prodea.App.sample.Controls.dashboard.addToCart();
	 
	});

		$("#btn_countDown").click(function (){
		var myCountdown1 = new Countdown({time:316});
	 
	});
    },
    
	addToCart: function()	{
	  Prodea.UI.Dialog.Alert('Add To Cart....');
	},
    updateDashboard: function() {
        var dfr = new $.Deferred();
    
        this.isBusy(true);
    
        $.when(    
            // Load some data for the dashboard view here.
        )
        .done( $.proxy( function() {
            // Update the dashboard view markup with the data here.
            dfr.resolve();
        }, this) )
        .always( $.proxy( function() {
            this.isBusy(false);
        }, this) )
        .fail( function(jqXHR, textStatus, errorThrown) {

            dfr.reject();
        });
        
        return dfr.promise();
    },
    
    _focus: function() {
		Prodea.Nav.focus("#button");
    }

});
